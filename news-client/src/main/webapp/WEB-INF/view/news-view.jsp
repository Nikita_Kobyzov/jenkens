<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<head>
  <title>News</title>
  <link rel="stylesheet" href="resources/css/bootstrap.min.css">
  <script src="resources/js/jquery.min.js"></script>
  <script src="resources/js/bootstrap.min.js"></script>
  <script src="resources/js/script.js"></script>
</head>
<body>
<div class="row text-center">
  <br>
  <div class="row">
    <div class="col-lg-8">
      <h1>News Portal</h1>
    </div>
    <div class="col-lg-1">
      <br>
    </div>
  </div>
  <hr color="black" size="10px">
</div>

<div class="row">
  <div class="col-sm-4 text-center">
    <ul class="nav nav-pills nav-stacked">
      <c:url var="href_hope_page" value="/client" context="${pageContext.request.contextPath}">
        <c:param name="command" value="home-page"/>
      </c:url>
      <li><a href="<c:out value="${href_hope_page}"/>">Main page</a></li>
    </ul>
  </div>
  <div class="col-lg-4">

    <h2>${news_to.news.title} (by ${news_to.author.author_name})</h2>
    <c:forEach items="${news_to.tagList}" var="tag">
                <span>
                   <kbd> ${tag.tag_name};</kbd>
                </span>
    </c:forEach>
    <p>${news_to.news.full_text}</p>
    <hr>
    <nav>
      <ul class="pager">
        <c:if test="${pager.previous_id != null}">
          <c:url var="href_previous" value="/client" context="${pageContext.request.contextPath}">
            <c:param name="command" value="news-view"/>
            <c:param name="news_id" value="${pager.previous_id}"/>
            <c:forEach items="${criteria.authorList}" var="id">
              <c:param name="authorList" value="${id}"/>
            </c:forEach>
            <c:forEach items="${criteria.tagList}" var="id">
              <c:param name="tagList" value="${id}"/>
            </c:forEach>
          </c:url>
          <li><a href="<c:out value="${href_previous}"/>">Previous</a></li>
        </c:if>
        <c:if test="${pager.next_id != null}">
          <c:url var="href_next" value="/client" context="${pageContext.request.contextPath}">
            <c:param name="command" value="news-view"/>
            <c:param name="news_id" value="${pager.next_id}"/>
            <c:forEach items="${criteria.authorList}" var="id">
              <c:param name="authorList" value="${id}"/>
            </c:forEach>
            <c:forEach items="${criteria.tagList}" var="id">
              <c:param name="tagList" value="${id}"/>
            </c:forEach>
          </c:url>
          <li><a href="<c:out value="${href_next}"/>">Next</a></li>
        </c:if>

      </ul>
    </nav>
    <hr>

    <form id="comment-add" action="<c:url value="/client" context="${pageContext.request.contextPath}"/>" method="POST">
      <input type="hidden" name="command" value="comment-add">
      <input type="hidden" name="news_id" value="${news_to.news.news_id}">
      <c:if test="${param.error != null}">
        <div class="alert-danger form-group">
          Comment text must be between 1 and 30 characters long
        </div>
      </c:if>
      <div class="form-group">

        <textarea name="comment_text" form="comment-add" class="form-control" rows="3"></textarea>
      </div>
      <button type="submit"  class="btn btn-default">
        <span class="glyphicon glyphicon-comment"></span> Add comment
      </button>

    </form>
    <c:forEach items="${news_to.commentList}" var="comment">
            <span>
                <fmt:formatDate value="${comment.creation_date}" pattern="MM/dd/YYYY"/>
            </span>

      <div class="row alert alert-info">
        <div class="col-lg-11">
          <p class="text-left">${comment.comment_text}</p>
        </div>
        <div class="col-sm-1">
        </div>
      </div>
    </c:forEach>
  </div>
</div>


<div class="row text-center">
  &copy; Nikita Kobyzov
</div>

</body>
</html>