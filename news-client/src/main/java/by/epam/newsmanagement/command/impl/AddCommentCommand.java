package by.epam.newsmanagement.command.impl;

import by.epam.newsmanagement.command.ICommand;
import by.epam.newsmanagement.command.request_parser.RequestParser;
import by.epam.newsmanagement.command.request_parser.impl.NewsIdParser;
import by.epam.newsmanagement.domain.Comment;
import by.epam.newsmanagement.exception.ClientCommandException;
import by.epam.newsmanagement.exception.CommandException;
import by.epam.newsmanagement.service.ICommentService;
import by.epam.newsmanagement.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Component("comment-add")
public class AddCommentCommand implements ICommand {

    private static final String VIEW_PAGE = "client?command=news-view&news_id=";
    private static final String ERROR_KEY = "&error";
    @Autowired
    private ICommentService commentService;
    @Autowired
    private RequestParser<Comment> commentRequestParser;

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        Comment comment = commentRequestParser.parse(request);
        String comment_text = comment.getComment_text();
        long news_id = comment.getNews_id();
        if(comment_text.length()>30||comment_text.isEmpty()){
            return VIEW_PAGE + news_id + ERROR_KEY;
        }
        Date current_date = new Date();
        comment.setCreation_date(current_date);
        try {
            commentService.addComment(comment);
        }catch (ServiceException e) {
            e.printStackTrace();
        }
        return VIEW_PAGE + news_id;
    }
}
