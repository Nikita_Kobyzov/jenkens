package by.epam.newsmanagement.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.epam.newsmanagement.dao.IRoleDao;
import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.domain.Role;
import by.epam.newsmanagement.service.IRoleService;
import by.epam.newsmanagement.service.exception.ServiceException;

/**
 * <p>
 * This class is designed to solve problems of business logic for a Role-entity
 * </p>
 * 
 * @author Mikita_Kobyzau
 *
 */
@Service("roleService")
public class RoleServiceImpl implements IRoleService {

	@Autowired
	private IRoleDao roleDao;

	@Override
	public void addRole(Role role) throws ServiceException {
		try {
			roleDao.create(role);
		} catch (DaoException e) {
			throw new ServiceException("Exception in Role Service with role = " + role, e);
		}

	}

}
