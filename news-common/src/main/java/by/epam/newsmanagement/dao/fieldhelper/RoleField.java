package by.epam.newsmanagement.dao.fieldhelper;

/**
 * <p>
 * This class contains the names of the role field
 * </p>
 * 
 * @author Mikita_Kobyzau
 *
 */
public final class RoleField {

	public static final String ROLE_ID = "RLS_ID";
	public static final String USER_ID = "RLS_USER_ID";
	public static final String ROLE_NAME = "RLS_NAME";
}
