package by.epam.newsmanagement.dao;

import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.domain.User;

/**
 * <p>
 * This interface describes the basic methods for DAO that work with User-entity
 * </p>
 *
 * @author Nikita Kobyzov
 * @see IGenericDao
 * @see by.epam.newsmanagement.dao.oracle.UserDaoImpl
 */
public interface IUserDao extends IGenericDao<Long, User> {

    /**
     * <p>
     * This method reads the user on a login
     * </p>
     *
     * @param login is one of unique identifiers of the user
     * @return the user corresponding to the given login or <tt>null</tt> if
     * there isn't entity with the appropriate login
     * @throws DaoException if the error occurred in working with the data
     */
    User readOnLogin(String login) throws DaoException;
}
