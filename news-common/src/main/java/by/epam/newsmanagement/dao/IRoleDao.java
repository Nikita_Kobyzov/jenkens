package by.epam.newsmanagement.dao;

import by.epam.newsmanagement.domain.Role;

/**
 * <p>
 * This interface describes the basic methods for DAO that work with Role-entity
 * </p>
 *
 * @author Nikita Kobyzov
 * @see IGenericDao
 * @see by.epam.newsmanagement.dao.oracle.RoleDaoImpl
 */
public interface IRoleDao extends IGenericDao<Long, Role> {

}
