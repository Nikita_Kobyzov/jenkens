package by.epam.newsmanagement.dao.oracle;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import by.epam.newsmanagement.dao.fieldhelper.PagerField;
import by.epam.newsmanagement.domain.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import by.epam.newsmanagement.dao.INewsDao;
import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.dao.fieldhelper.NewsField;
import by.epam.newsmanagement.domain.SearchCriteria;
import by.epam.newsmanagement.dao.sql.SqlRequest;
import by.epam.newsmanagement.domain.News;

/**
 * <p>
 * This class works with the entity of the news from the Oracle database
 * </p>
 *
 * @author Mikita_Kobyzau
 */
@Component
public class NewsDaoImpl implements INewsDao {

    @Autowired
    private DataSource dataSource;

    @Override
    public Long create(News entity) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_CREATE_NEWS,
                new String[]{NewsField.NEWS_ID})) {
            ps.setString(1, entity.getTitle());
            ps.setString(2, entity.getShort_text());
            ps.setString(3, entity.getFull_text());
            ps.setTimestamp(4, new Timestamp(entity.getCreation_date().getTime()));
            ps.setDate(5, new Date(entity.getModification_date().getTime()));
            ps.executeUpdate();
            try (ResultSet rs = ps.getGeneratedKeys()) {
                rs.next();
                entity.setNews_id(rs.getLong(1));
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database with entity = " + entity, e);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with entity = " + entity, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return entity.getNews_id();
    }

    @Override
    public News read(Long key) throws DaoException {
        News news;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_GET_NEWS_ON_ID)) {
            ps.setLong(1, key);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    news = this.getNewsFromResultSet(resultSet);
                } else {
                    return null;
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database with key = " + key, e);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with key = " + key, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return news;
    }

    @Override
    public List<News> read() throws DaoException {
        List<News> newsList = new ArrayList<News>();
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(SqlRequest.SQL_GET_ALL_NEWS)) {
            while (rs.next()) {
                newsList.add(this.getNewsFromResultSet(rs));
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return newsList;
    }

    @Override
    public boolean update(Long key, News entity) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_UPDATE_NEWS)) {
            ps.setString(1, entity.getTitle());
            ps.setString(2, entity.getShort_text());
            ps.setString(3, entity.getFull_text());
            ps.setDate(4, new Date(entity.getModification_date().getTime()));
            ps.setLong(5, key);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with key = "
                    + key + ", entity = " + entity, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean delete(Long key) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_DELETE_NEWS)) {
            ps.setLong(1, key);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with key = " + key, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public Long countOfNews(SearchCriteria criteria) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        String query = SqlRequest.SQL_GET_COUNT_OF_NEWS + getSearchCriteriaQuery(criteria)
                + SqlRequest.CLOSE_BRACKET;
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            int parameterIndex = 1;
            for (Long id : criteria.getTagList()) {
                ps.setLong(parameterIndex++, id);
            }
            if (!criteria.getTagList().isEmpty()) {
                ps.setInt(parameterIndex++, criteria.getTagList().size());
            }
            for (Long id : criteria.getAuthorList()) {
                ps.setLong(parameterIndex++, id);
            }
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getLong(1);
                } else {
                    throw new DaoException("Empty ResultSet");
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database with"
                        + " criteria = " + criteria, e);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with" +
                    " criteria = " + criteria, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public List<News> readByCriteria(Long page, Integer newsPerPage, SearchCriteria criteria) throws DaoException {
        List<News> newsList = new ArrayList<News>();
        int parameterIndex = 1;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        String query = SqlRequest.SQL_NEWS_PAGINATION_LEFT_QUERY + getSearchCriteriaQuery(criteria)
                + SqlRequest.SQL_NEWS_PAGINATION_RIGHT_QUERY;
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            long first_news_position = newsPerPage * (page - 1) + 1;
            long last_news_position = page * newsPerPage;
            for (Long id : criteria.getTagList()) {
                ps.setLong(parameterIndex++, id);
            }
            if (!criteria.getTagList().isEmpty()) {
                ps.setInt(parameterIndex++, criteria.getTagList().size());
            }
            for (Long id : criteria.getAuthorList()) {
                ps.setLong(parameterIndex++, id);
            }

            ps.setLong(parameterIndex++, last_news_position);
            ps.setLong(parameterIndex++, first_news_position);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    newsList.add(this.getNewsFromResultSet(rs));
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database with page = "
                        + page + ", newsPerPage = " + newsPerPage + ", criteria = " + criteria, e);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with page = "
                    + page + ", newsPerPage = " + newsPerPage + ", criteria = " + criteria, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return newsList;
    }

    @Override
    public boolean connectNewsWithTag(long news_id, long tag_id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_CREATE_NEWS_TAG)) {
            ps.setLong(1, news_id);
            ps.setLong(2, tag_id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with news_id = "
                    + news_id + ", tag_id = " + tag_id, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean connectNewsWithAuthor(long news_id, long author_id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_CREATE_NEWS_AUTHOR)) {
            ps.setLong(1, news_id);
            ps.setLong(2, author_id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with news_id = "
                    + news_id + ", author_id = " + author_id, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public boolean deleteNewsTag(long news_id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_DELETE_NEWS_TAG_ON_NEWS_ID)) {
            ps.setLong(1, news_id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with news_id = " + news_id, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    @Override
    public Pager getPager(long news_id, SearchCriteria criteria) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        String query = SqlRequest.SQL_PAGER_LEFT_QUERY + getSearchCriteriaQuery(criteria)
                + SqlRequest.SQL_PAGER_RIGHT_QUERY;
        try (PreparedStatement ps = connection.prepareStatement(query)) {
            int parameterIndex = 1;
            for (Long id : criteria.getTagList()) {
                ps.setLong(parameterIndex++, id);
            }
            if (!criteria.getTagList().isEmpty()) {
                ps.setInt(parameterIndex++, criteria.getTagList().size());
            }
            for (Long id : criteria.getAuthorList()) {
                ps.setLong(parameterIndex++, id);
            }
            ps.setLong(parameterIndex++, news_id);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    Pager pager = new Pager();
                    pager.setNext_id(rs.getLong(PagerField.NEXT_ID));
                    if(rs.wasNull()){
                        pager.setNext_id(null);
                    }
                    pager.setPrevious_id(rs.getLong(PagerField.PREVIOUS_ID));
                    if(rs.wasNull()){
                        pager.setPrevious_id(null);
                    }
                    return pager;
                } else {
                    throw new DaoException("Empty ResultSet");
                }
            } catch (SQLException e) {
                throw new DaoException("Exception in working with the database with"
                        + " criteria = " + criteria, e);
            }
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with" +
                    " criteria = " + criteria, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    @Override
    public boolean deleteNewsAuthor(long news_id) throws DaoException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement ps = connection.prepareStatement(SqlRequest.SQL_DELETE_NEWS_AUTHOR)) {
            ps.setLong(1, news_id);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Exception in working with the database with news_id = " + news_id, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return true;
    }

    /**
     * <p>
     * This method create a query from search criteria
     * </p>
     *
     * @param criteria is a object that contains the search criteria
     * @return a search query
     */
    private String getSearchCriteriaQuery(SearchCriteria criteria) {
        StringBuilder sb = new StringBuilder();
        sb.append(SqlRequest.SQL_GET_ALL_NEWS_ON_CRITERIA);
        boolean isTagAdded = false;
        if (criteria.getTagList() != null && !criteria.getTagList().isEmpty()) {
            isTagAdded = true;
            sb.append(SqlRequest.WHERE_PHRASE);
            sb.append(SqlRequest.SQL_SEARCH_BY_TAGS_LEFT_QUERY);
            for (int i = 0; i < criteria.getTagList().size(); i++) {
                sb.append(SqlRequest.WILD_CARD + SqlRequest.COMMA);
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append(SqlRequest.SQL_SEARCH_BY_TAGS_RIGHT_QUERY);
        }
        if (criteria.getAuthorList() != null && !criteria.getAuthorList().isEmpty()) {
            if (isTagAdded) {
                sb.append(SqlRequest.AND_PHRASE + SqlRequest.OPEN_BRACKET);
            } else {
                sb.append(SqlRequest.WHERE_PHRASE);
            }
            for (int i = 0; i < criteria.getAuthorList().size(); i++) {
                sb.append(SqlRequest.BASIC_QUERY_BY_AUTHORS + SqlRequest.WILD_CARD);
                sb.append(SqlRequest.OR_PHRASE);
            }
            sb.delete(sb.length() - 3, sb.length() - 1);
            if (isTagAdded) {
                sb.append(SqlRequest.CLOSE_BRACKET);
            }
        }
        sb.append(SqlRequest.SQL_SORTING_BY_COMMENTS_AND_MODIFICATION_DATE_QUERY);
        return sb.toString();
    }

    /**
     * <p>
     * This method reads one record from the {@link java.sql.ResultSet}
     * </p>
     *
     * @param rs is an resulting query
     * @return an object of class {@link by.epam.newsmanagement.domain.News}
     * @throws SQLException if the error occurred in working with the database
     */
    private News getNewsFromResultSet(ResultSet rs) throws SQLException {
        News news = new News();
        news.setNews_id(rs.getLong(NewsField.NEWS_ID));
        news.setTitle(rs.getString(NewsField.TITLE));
        news.setShort_text(rs.getString(NewsField.SHOTR_TEXT));
        news.setFull_text(rs.getString(NewsField.FULL_TEXT));
        news.setCreation_date(rs.getTimestamp(NewsField.CREATION_DATE));
        news.setModification_date(rs.getTimestamp(NewsField.MODIFICATION_DATE));
        return news;
    }

}
