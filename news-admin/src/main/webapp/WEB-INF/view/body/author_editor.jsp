<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<div class="row">
    <div class="col-sm-1">
    </div>
    <div class="col-lg-11">
        <c:url var="auth_add" value="/author/add" context="${pageContext.request.contextPath}"/>
        <sf:form modelAttribute="author" action="${auth_add}" cssClass="form-inline" method="post">
            <sf:input path="author_name" cssClass="form-control"/>
            <input type="submit" class="btn btn-info" value="Add"><br>
            <sf:errors path="author_name" cssStyle="color: #f39c12"/><br>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </sf:form>
        <c:forEach var="author" items="${authorList}">
            <sf:form cssClass="form-inline" method="post"
                     modelAttribute="author" id="updateAuthor${author.author_id}">
                <sf:hidden path="author_id" value="${author.author_id}"/>
                <span>
                     <c:if test="${author.expired == null}">
                         <sf:input id="name${author.author_id}" value="${author.author_name}" cssClass="form-control"
                                   readonly="true" path="author_name"></sf:input>

                         <a id="edit${author.author_id}" href="#" onclick="view(${author.author_id})">edit</a>
                         <span id="buttons${author.author_id}" hidden="">
                            <a href="#" onclick="updateAuthor(${author.author_id})">update</a>
                            <a href="#" onclick="expireAuthor(${author.author_id})">expire</a>
                            <a href="#" onclick="hide(${author.author_id})">cancel</a>
                         </span>
                     </c:if>
                    <c:if test="${author.expired != null}">
                        <sf:input path="author_name" class="form-control" readonly="true"
                                  value="${author.author_name}"/>
                        <span> Is expired since <fmt:formatDate value="${author.expired}" pattern="MM/dd/yyyy"/></span>
                    </c:if>

        </span>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </sf:form>
            <c:url var="auth_expire" value="/author/expire" context="${pageContext.request.contextPath}"/>
            <sf:form method="post" action="${auth_expire}" modelAttribute="author"
                     id="expireAuthor${author.author_id}">
                <sf:hidden path="author_id" value="${author.author_id}"/>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </sf:form>
        </c:forEach>
    </div>
    <br><br><br>
</div>